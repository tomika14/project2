<?php
$uzenet = "";
if(isset($_GET["err"])){
    $uzenet = "Hibás felhasználónév vagy jelszó!";
}
?>

<!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
</head>
<body>
<p>
    <?=$uzenet;?>
</p>
<kbd>Email: admin@admin.hu | Pass: admin123</kbd>
<form action="auth" method="post">
    <div>
    <label for="email">E-mail cím:</label>
    <input type="email" name="email" id="email">
    </div>
    <div>
    <label for="password">Jelszó:</label>
    <input type="password" name="password" id="password">
    </div>
    <div>
    <button type="submit">Bejelentkezés</button>
    </div>
</form>
</body>
</html>
