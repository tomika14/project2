<?php

namespace feladat;

class Security
{

    private $users;
    private $dbPath = "./db/user.txt";

    public function __construct()
    {
        $this->users = $this->getUsersFromFile();
    }

    public function getUsers()
    {
        return $this->users;
    }

    public function getUsersFromFile()
    {
        $read = file_get_contents($this->dbPath);
        $lines = explode("\n", $read);
        $i = 0;
        $users = array();
        foreach($lines as $value)
        {
            $cols[$i] = explode("\t", $value);
            $cols[$i][1] = trim($cols[$i][1]);
            $u = new User($cols[$i][0],$cols[$i][1]);

            $users[$i] = $u;
            $i++;
        }
        return $users;
    }


}