<?php

namespace feladat;

class Lottery
{

    private array $randomnumbers;

    public function __construct()
    {
        $this->randomnumbers = array();
    }

    public function generateRandomNumbers()
    {
        for($i = 0; $i < 5; $i++)
        {
            $random = mt_rand(1, 90);
            if ($this->checkIfExistsInArray($random)) {
                $this->generateRandomNumbers();
            } else {
                array_push($this->randomnumbers, $random);
            }
        }
    }

    public function getRandomnumbers()
    {
        return $this->randomnumbers;
    }

    private function checkIfExistsInArray($num): bool
    {
        for($i = 0; $i < count($this->randomnumbers); $i++)
        {
            if($num == $this->randomnumbers[$i])
            {
                return true;
            }
        }
        return false;
    }

}