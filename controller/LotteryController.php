<?php

namespace feladat;

class LotteryController
{
    public $refreshNumber;
    public $randomNumbers;

    public function __construct()
    {
        $this->refreshNumber = $this->checkRefreshNumber();
        $this->showNumbers();
    }

    private function checkRefreshNumber()
    {
        if(!isset($_SESSION["lottery"])){
            $_SESSION["lottery"] = 1;
            return 1;
        }
        else
        {
            if($_SESSION["lottery"] < 5) {
                $_SESSION["lottery"] += 1;
                return $_SESSION["lottery"];
            }else
            {
                $_SESSION["lottery"] = 1;
                return 1;
            }
        }
    }

    public function showNumbers()
    {
        if($this->getRefreshNumber() == 1){
            $lottery = new Lottery();
            $lottery->generateRandomNumbers();
            $this->setRandomNumbers($lottery->getRandomnumbers());
            $_SESSION["numbers"] = $this->getRandomNumbers();
        }
    }

    public function setRandomNumbers($randomNumbers): void
    {
        $this->randomNumbers = $randomNumbers;
    }

    public function getRandomNumbers()
    {
        return $this->randomNumbers;
    }

    public function getRefreshNumber()
    {
        return $this->refreshNumber;
    }
    public function getStringOfRandomNumbers()
    {
        $resp = "";
        for($i = 0; $i < 5; $i++)
        {
            $resp .= $_SESSION["numbers"][$i]. ", ";
        }
        $resp = rtrim($resp,", ");
        return $resp;
    }


}