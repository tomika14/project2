<?php

namespace feladat;
class SecurityController
{


    private $users;

    public function __construct()
    {
        $sec = new Security();
        $this->users = $sec->getUsers();
    }

    public function loginMethod(User $user): bool
    {
        for ($i = 0; $i < count($this->users); $i++)
        {

            $u = $this->users[$i];
            if ($u->getEmail() == $user->getEmail())
            {
                if(password_verify($user->getPassword(),$u->getPassword()))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        return false;
    }

    public function checkSession(): bool
    {
        if(isset($_SESSION["loggedin"]))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}