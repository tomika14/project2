<?php

use feladat\SecurityController;
use feladat\User;
$sec = new SecurityController();
$bool = $sec->checkSession();

$uzenet = "";
if(($_SERVER["REQUEST_METHOD"]) == "POST")
{
    $sec = new SecurityController();
    $user = new User($_POST["email"],$_POST["password"]);
    $login = $sec->loginMethod($user);

    if($login)
    {
        $_SESSION["loggedin"] = true;
        header("Location: lottery");
        exit;
    }
    else
    {
        header("Location: login?err=1");
        exit;
    }
}

if(!$bool)
{
    header("Location: login");
    exit;
}