<?php
session_start();
foreach (glob("model/*.php") as $filename)
{
    include $filename;
}
foreach (glob("controller/*.php") as $filename)
{
    $file = explode("/",$filename)[1];

    include $filename;
}
if(!isset($_GET["get"]))
{
    header("Location: lottery");
    exit;
}
switch ($_GET["get"])
{
    case "":
    case "index":
    case "auth":
        include "auth.php";
        break;
    case "login":
        include "login.php";
        break;
    case "content":
    case "lottery":
        include "auth.php";
        include "content.php";
        break;
    case "logout":
        include "logout.php";
        break;
}

